import 'package:flutter/material.dart';
import 'package:flutter_meal_app/data/dummy_data.dart';
import 'package:flutter_meal_app/models/meal.dart';
import 'package:flutter_meal_app/screens/filter_screen.dart';
import 'package:flutter_meal_app/screens/meal_detail_screen.dart';
import 'package:flutter_meal_app/screens/tab_screen.dart';
import 'screens/category_meals_screen.dart';

import 'screens/categories_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filters = {
    'glutenFree': false,
    'vegetarian': false,
    'vegan': false,
    'lactoseFree': false,
  };

  List<Meal> _availableMeal = DUMMY_MEALS;
  List<Meal> _favoriteMeal = [];
  void _setFilters(Map<String, bool> filtersData){
      setState(() {
        _filters = filtersData;
        _availableMeal = DUMMY_MEALS.where((element) {
            if(_filters['glutenFree'] && !element.isGlutenFree){
              return false;
            }
            if(_filters['vegetarian'] && !element.isVegetarian ){
              return false;
            }
            if(_filters['vegan'] && !element.isVegan){
              return false;
            }
            if(_filters['lactoseFree'] && !element.isLactoseFree){
              return false;
            }
            return true;
        }).toList();
      });
  }

  void _toggleFav(String mealId){
    final existingIndex= _favoriteMeal.indexWhere((element) => element.id == mealId);
    // if index is greater than 0 then its added or available
    if(existingIndex >= 0){
      setState(() {
    _favoriteMeal.removeAt(existingIndex);
      });
    }else{
      setState(() {
        _favoriteMeal.add(DUMMY_MEALS.firstWhere((element) => element.id == mealId));
      });

    }
  }

  bool isMealFev(String mealId){
    return _favoriteMeal.any((element) => element.id == mealId);
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DeliMeals',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        accentColor: Colors.amber,
        canvasColor: Color.fromRGBO(255, 255, 255, 5),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
            body1: TextStyle(color: Color.fromRGBO(20, 51, 51, 1)),
            body2: TextStyle(color: Color.fromRGBO(20, 51, 51, 1)),
            title: TextStyle(
                fontSize: 20,
                fontFamily: 'RobotoCondensed',
                fontWeight: FontWeight.bold)),
      ),
      initialRoute: '/',
      // default route is /

      //home: CategoriesScreen(),
      routes: {
        '/': (ctx) => TabScreen(_favoriteMeal),
        CategoryMealsScreen.routeName: (ctx) => CategoryMealsScreen(_availableMeal),
        MealDetailScreen.routeName: (ctx) => MealDetailScreen(_toggleFav,isMealFev),
        FilterScreen.routeName: (ctx) => FilterScreen(_filters,_setFilters),
      },
      onGenerateRoute: (settings) {
        print('return the route $settings.arguments');
        //if(settings.name == 'meals-item')
        return MaterialPageRoute(builder: (ctx) => CategoryMealsScreen(_availableMeal));
      },
      onUnknownRoute: (settings) {
        //print('fallback error');
        return MaterialPageRoute(builder: (ctx) => CategoryMealsScreen(_availableMeal));
      },
    );
  }
}
