import 'package:flutter/material.dart';
import 'package:flutter_meal_app/models/meal.dart';
import 'package:flutter_meal_app/widgets/meal_item.dart';

class FavouriteScreen extends StatelessWidget {

  final List<Meal> favMeal;

  FavouriteScreen(this.favMeal);

  @override
  Widget build(BuildContext context) {
    if(favMeal.isEmpty){
      return Center(child: Text('You have no favorite yet. Start adding some!!'),);
    }else {
      return ListView.builder(
          itemCount: favMeal.length,
          itemBuilder: (ctx, index) {
            return MealItem(
                favMeal[index].id,
                favMeal[index].title,
                favMeal[index].imageUrl,
                favMeal[index].duration,
                favMeal[index].complexity,
                favMeal[index].affordability,
                );
          });
    }
  }
}
