import 'package:flutter/material.dart';
import '../data/dummy_data.dart';
import '../widgets/category_item.dart';

class CategoriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GridView(
        children: DUMMY_CATEGORIES.map((catData) {
          return  CategoryItem(catData.id, catData.title, catData.color);
        }

        ).toList(),
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          childAspectRatio: 2/1,
          crossAxisSpacing: 20,
          mainAxisSpacing: 10,
        ),
        padding: const EdgeInsets.all(25),
      ),
    );
  }
}
