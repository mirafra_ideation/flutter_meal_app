import 'package:flutter/material.dart';
import 'package:flutter_meal_app/widgets/main_drawer.dart';

class FilterScreen extends StatefulWidget {
  final Function saveFilters;
  final Map<String,bool>  currentFilter;
  static const routeName = '/filter';


  FilterScreen(this.currentFilter,this.saveFilters);

  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _glutenFree = widget.currentFilter['glutenFree'];
    _vegetarian = widget.currentFilter['vegetarian'];
    _vegan = widget.currentFilter['vegan'];
    _lactoseFree = widget.currentFilter['lactoseFree'];
  }
  var _glutenFree = false;
  var _vegetarian = false;
  var _vegan = false;
  var _lactoseFree = false;

  Widget buildSwitchList(BuildContext context, String title, String subTitle,
      bool itemValue, Function updateValue) {
    return SwitchListTile(
      value: itemValue,
      onChanged: updateValue,
      title: Text(title),
      subtitle: Text(subTitle),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: MainDrawer(),
        appBar: AppBar(
          title: Text('Filter'),
          actions: [
            IconButton(
              icon: Icon(Icons.save),
              onPressed: (){
                final selectedFilters =  {
                  'glutenFree': _glutenFree,
                  'vegetarian': _vegetarian,
                  'vegan': _vegan,
                  'lactoseFree': _lactoseFree,
                };
                widget.saveFilters(selectedFilters);
              },
            )
          ],
        ),
        body: Column(
          children: [
            Container(
              padding: EdgeInsets.all(20),
              child: Text(
                'Adjust your meal selection',
                style: Theme.of(context).textTheme.title,
              ),
            ),
            Expanded(
                child: ListView(
              children: [
                buildSwitchList(context, 'Gluten-Free',
                    'Only Gluten free food items', _glutenFree, (newVal) {
                  setState(() {
                    _glutenFree = newVal;
                  });
                }),
                buildSwitchList(
                    context, 'Vegetarian', 'Only Veg food items', _vegetarian,
                    (newVal) {
                  setState(() {
                    _vegetarian = newVal;
                  });
                }),
                buildSwitchList(
                    context, 'Vegan', 'Only vegan food items', _vegan,
                    (newVal) {
                  setState(() {
                    _vegan = newVal;
                  });
                }),
                buildSwitchList(context, 'LactoseFree',
                    'Only lactoseFree food items', _lactoseFree, (newVal) {
                  setState(() {
                    _lactoseFree = newVal;
                  });
                }),
              ],
            ))
          ],
        ));
  }
}
